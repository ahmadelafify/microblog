from flask import current_app, url_for

from app.models import Post


def get_posts_data(page) -> dict:
    posts = Post.query.order_by(Post.timestamp.desc()).paginate(
        page, current_app.config['POSTS_PER_PAGE'], False)
    next_url = url_for('main.index', page=posts.next_num) \
        if posts.has_next else None
    prev_url = url_for('main.index', page=posts.prev_num) \
        if posts.has_prev else None
    return dict(posts=posts.items, next_url=next_url, prev_url=prev_url)